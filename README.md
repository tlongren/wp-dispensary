![WP Dispensary logo](https://www.wpdispensary.com/wp-content/uploads/2015/10/wpdispensary-logo.png)

# WP Dispensary

The world's first (and best) medical marijuana dispensary menu plugin for WordPress.

### Features Overview

WP Dispensary was built with ease of use in mind, so the setup process of the plugin is as simple as adding and activating the plugin, adding your content and sitting back to bask in the glory of how easy it was to set up.

There's also a lot of cool things baked right into the WP Dispensary menu plugin.

### Easy to manage menu

We've made every effort to have the adding and editing of your dispensary menu content as seamless as possible with the normal WordPress experience.

This means that each menu category has it's own section in the WordPress dashboard and content can be added just like you would normally add content with posts or pages.

When you log into your WordPress dashboard, you will also be able to see an overview of your dispensary menu in the at a glance box, letting you quickly see how many items are in your dispensary menu.

But that's not all...

### Complete control over menu item details

With the WP Dispensary menu plugin, you'll be able to have 100% control over all of the information that you would like to include for each of your menu items.

Let your patients easily see menu items based on the type of effects they have, as well as what flowers are good for specific symptoms. Patients can also find menu items based on flavor, aroma and specific conditions.

You can also include the pricing for each item as well, directly from the content editor, giving you the ease of editing menu prices whenever you have a sale, run out of certain items, etc.

But wait, there's more...

### Your content is exportable

One thing that we wanted to make sure of when creating this plugin was that you have the ability to easily export your content at any time.

Luckily, WordPress comes built with a great content exporter, and we've included your menu content in the export options so you can instantly have your dispensary menu backed up.

If you ever need to move your website to another server, or would just like to have a backup of the current content, you're able to easily export your dispensary menu content with WordPress' built in export tool.

### 100% Open Source

WP Dispensary is free to use with no license restrictions or expensive setup fees. It's as simple as downloading the plugin, installing it on your WordPress powered website, and adding content.

We believe in the power of open source, and this plugin is a shining example of that. Not only can you download and freely use this plugin as you see fit, developers are free to contribute to the [code on Github](https://www.github.com/deviodigital/wp-dispensary/)

This means that more eyes are looking at the code base to make sure everything is built with WordPress standards in mind, giving you the peace of mind that the plugin you're using is of the highest quality possible.

### Dispensary Display

The WP Dispensary menu plugin is the first step to getting your menu live online. Once you have the plugin installed, you'll need an easy way to display your menu, and that's where the Dispensary Display WordPress theme comes into play.

**Get the most out of WP Dispensary**

We've personally crafted a WordPress theme that is fully integrated with the WP Dispensary plugin, giving you a website that showcases your menu in the most beautiful way possible.

The best part is, since we personally built and maintain this theme, you no longer have to worry about plugin conflicts every time there's an update released. Instead, you're able to focus on your patients, knowing that we've got your online menu completely covered.

You can view more information about this theme, as well as the live demo at the [offical WP Dispensary website](https://www.wpdispensary.com)

### Extend WP Dispensary

When building this plugin, we made sure to keep it lean, including only the core needs of every menu. While this works well in most cases, we're aware there are additional needs that dispensary owners will need to address, which is where the add-on's come in.

With our free and commercial add-on's, you'll be able to extend the functionality of the WP Dispensary menu plugin, giving your website the competitive edge in the growing medical marijuana market.

You can view our current add-on's on the [offical WP Dispensary website](https://www.wpdispensary.com)

## Installation

1. Go to `Plugins - Add New` in your WordPress admin panel and search for "WP Dispensary"
2. Install and activate the plugin directly in your admin panel
3. Pat yourself on the back for a job well done :)
